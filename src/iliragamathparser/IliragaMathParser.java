/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iliragamathparser;

import interfaces.*;
import java.util.*;

/**
 *
 * @author uide8178
 */
public class IliragaMathParser {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Map<String, Integer> inputValues = new HashMap<>();
        
        // this are the variables
        inputValues.put("a", 1);
        inputValues.put("b", 0);
        
        inputValues.put("c", 1);
        inputValues.put("d", 1);
        
        // the formula at this moment can contain two operators: OR / AND
        // brackets are handled properly... because of the way the lexer is implemented
        // they need a empty space after the char, otherwhise they're not seen by the compiler
        // NOT operator is supported by the VM but not by the compiler
        int result = new CalculatorFactory().CalculateFormula("( a AND b ) OR ( c AND d ) AND ( a OR b )", inputValues);
        
        // show output
        System.out.println(String.format("VM executed formula, result was %d", result));
    }
}

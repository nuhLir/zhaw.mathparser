/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author uide8178
 */
public interface ICompiler {
    /**
     * Compiles the formula with the input into executable bytecode
     * 
     * @param formula
     * @param input
     * 
     * @return the formula output
     */
    ArrayList<Integer> Compile(String formula, Map<String, Integer> input);
}

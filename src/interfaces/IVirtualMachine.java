/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author uide8178
 */
public interface IVirtualMachine {
    /**
     * Executes the specified bytecode
     * 
     * @param byteCode
     * @return 
     */
    int Execute(ArrayList<Integer> byteCode);
}

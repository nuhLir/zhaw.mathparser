/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import core.*;
import java.util.*;

/**
 *
 * @author uide8178
 */
public class CalculatorFactory {
    /**
     * Calculates the specified formula
     * 
     * @param formula
     * @param input
     * @return 
     */
    public int CalculateFormula(String formula, Map<String, Integer> input){
        // do the actual compiling
        ArrayList<Integer> byteCode = getCompiler().Compile(formula, input);
        
        // execute compiled bytecode in virtual machine
        return getVirtualMachine().Execute(byteCode);
    }
    
    /**
     * Gets the compiler
     * @return 
     */
    private ICompiler getCompiler(){
        return new IliragaCompiler();
    }
    
    /**
     * Gets the Virtual Machine
     * 
     * @return 
     */
    private IVirtualMachine getVirtualMachine(){
        return new IliragaVirtualMachine();
    }
}

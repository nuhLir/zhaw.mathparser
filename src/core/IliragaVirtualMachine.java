/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import interfaces.IVirtualMachine;
import java.util.ArrayList;

/**
 *
 * @author uide8178
 */
public class IliragaVirtualMachine implements IVirtualMachine {
    private ArrayList<Integer> bytecode;
    private int currentBytecodeIndex;
    
    // these commands are used for the VM
    // while executing the compiled bytecode
    private final int byteCodeAnd = -15;
    private final int byteCodeOr = -16;
    private final int byteCodeNot = -17;
    
    /**
     * Executes the next token in the bytecode
     * 
     * @return 
     */
    private int execute(){
        currentBytecodeIndex++;
        
        int currentBytecode = bytecode.get(currentBytecodeIndex);
        
        switch (currentBytecode){
            case byteCodeAnd:
                return Math.min(execute(), execute());
            case byteCodeOr:
                return Math.max(execute(), execute());
            case byteCodeNot:
                return 1 - execute();
        }
        
        // if this code place was reached
        // this means the current bytecode entry
        // is atomic
        return currentBytecode;
    }
    
    @Override
    public int Execute(ArrayList<Integer> byteCode) {
        currentBytecodeIndex = -1;
        
        this.bytecode = byteCode;
        
        return execute();
    }
}
